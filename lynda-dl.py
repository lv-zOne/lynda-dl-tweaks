#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import time
import lynda
import argparse
import datetime
import requests

from pprint import pprint
from lynda import __version__
from lynda._colorized import *
from lynda._compat import pyver, re
from lynda._getpass import GetPass
from lynda._progress import ProgressBar
from lynda._colorized.banner import banner
getpass = GetPass()

class Lynda(ProgressBar):

	def __init__(self, url, username='', password='', organization='', cookies=''):
		self.url = url
		self.username = username
		self.password = password
		self.cookies  = cookies
		self.organization = organization
		super(Lynda, self).__init__()

	def course_list_down(self):
		if self.cookies:
			sys.stdout.write(fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Trying to login using cookies ...\n")
		if not self.cookies:
			if not self.organization:
				sys.stdout.write(fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Trying to login as " + fm + sb +"(%s)" % (self.username) +  fg + sb +"...\n")
			if self.organization:
				sys.stdout.write(fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Trying to login as organization " + fm + sb +"(%s)" % (self.organization) +  fg + sb +"...\n")
		course = lynda.course(url=self.url, username=self.username, password=self.password, organization=self.organization, cookies=self.cookies)
		course_id = course.id
		course_name = course.title
		chapters = course.get_chapters()
		total_lectures = course.lectures
		total_chapters = course.chapters
		assets = course.assets
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Course " + fb + sb + "'%s'.\n" % (course_name))
		sys.stdout.write (fc + sd + "[" + fm + sb + "+" + fc + sd + "] : " + fg + sd + "Chapter(s) (%s).\n" % (total_chapters))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Lecture(s) (%s).\n" % (total_lectures))
		for chapter in chapters:
			chapter_id = chapter.id
			chapter_index = chapter.index
			chapter_title = chapter.title
			lectures = chapter.get_lectures()
			lectures_count = chapter.lectures
			sys.stdout.write ('\n' + fc + sd + "[" + fw + sb + "+" + fc + sd + "] : " + fw + sd + "Chapter (%s-%s)\n" % (chapter_title, chapter_id))
			sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Lecture(s) (%s).\n" % (lectures_count))
			for lecture in lectures:
				lecture_id = lecture.id
				lecture_index = lecture.index
				lecture_title = lecture.title
				lecture_subtitles = lecture.subtitles
				lecture_best = lecture.getbest()
				lecture_streams = lecture.streams
				if lecture_streams:
					sys.stdout.write(fc + sd + "     - " + fy + sb + "duration   : " + fm + sb + str(lecture.duration)+ fy + sb + ".\n")
					sys.stdout.write(fc + sd + "     - " + fy + sb + "Lecture id : " + fm + sb + str(lecture_id)+ fy + sb + ".\n")
					for stream in lecture_streams:
						content_length = stream.get_filesize()
						if content_length != 0:
							if content_length <= 1048576.00:
								size = round(float(content_length) / 1024.00, 2)
								sz = format(size if size < 1024.00 else size/1024.00, '.2f')
								in_MB = 'KB' if size < 1024.00 else 'MB'
							else:
								size = round(float(content_length) / 1048576, 2)
								sz = format(size if size < 1024.00 else size/1024.00, '.2f')
								in_MB = "MB " if size < 1024.00 else 'GB '
							if lecture_best.dimention[1] == stream.dimention[1]:
								in_MB = in_MB + fc + sb + "(Best)" + fg + sd
							sys.stdout.write('\t- ' + fg + sd + "{:<23} {:<8}{}{}{}{}\n".format(str(stream), str(stream.dimention[1]) + 'p', sz, in_MB, fy, sb))
		if assets and len(assets) > 0:
			for asset in assets:
				content_length = asset.get_filesize()
				if content_length != 0:
					if content_length <= 1048576.00:
						size = round(float(content_length) / 1024.00, 2)
						sz = format(size if size < 1024.00 else size/1024.00, '.2f')
						in_MB = 'KB' if size < 1024.00 else 'MB'
					else:
						size = round(float(content_length) / 1048576, 2)
						sz = format(size if size < 1024.00 else size/1024.00, '.2f')
						in_MB = "MB " if size < 1024.00 else 'GB '
					sys.stdout.write('\t- ' + fg + sd + "{:<23} {:<8}{}{}{}{}\n".format(str(asset), asset.extension, sz, in_MB, fy, sb))

	def download_image(self, image_url, filename):
		imageContent = requests.get(image_url)
		if imageContent.status_code == 200:
			with open(filename, 'wb') as f:
				try:
					f.write(imageContent.content)
				except Exception as e:
					sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fr + sb + 'Python3 Exception when downloading image : {}'.format(e) + '\n')
				#else:
					#sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fr + sb + 'Image successfully downloaded\n')
			f.close()

	def download_assets(self, assets='', filepath=''):
		if assets:
			title = assets.filename
			sys.stdout.write(fc + sd + "\n[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Downloading asset(s)\n")
			sys.stdout.write(fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Downloading (%s)\n" % (title))
			try:
				retval = assets.download(filepath=filepath, quiet=True, callback=self.show_progress)
			except KeyboardInterrupt:
				sys.stdout.write (fc + sd + "\n[" + fr + sb + "-" + fc + sd + "] : " + fr + sd + "User Interrupted..\n")
				sys.exit(0)
			msg = retval.get('msg')
			if msg == 'already downloaded':
				sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Asset : '%s' " % (title) + fy + sb + "(already downloaded).\n")
			elif msg == 'download':
				sys.stdout.write (fc + sd + "[" + fm + sb + "+" + fc + sd + "] : " + fg + sd + "Downloaded  (%s)\n" % (title))
			else:
				sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Asset : '%s' " % (title) + fc + sb + "(download skipped).\n")
				sys.stdout.write (fc + sd + "[" + fr + sb + "-" + fc + sd + "] : " + fr + sd + "{}\n".format(msg))

	def download_subtitles(self, subtitle='', filepath=''):
		if subtitle:
			title = subtitle.title + '-' + subtitle.language
			filename = "%s\\%s" % (filepath, subtitle.filename) if os.name == 'nt' else "%s/%s" % (filepath, subtitle.filename)
			try:
				retval = subtitle.download(filepath=filepath)
			except KeyboardInterrupt:
				sys.stdout.write (fc + sd + "\n[" + fr + sb + "-" + fc + sd + "] : " + fr + sd + "User Interrupted..\n")
				sys.exit(0)

	def download_lectures(self, lecture_best='', lecture_title='', inner_index='', lectures_count='', filepath=''):
		if lecture_best:
			sys.stdout.write(fc + sd + "\n[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Lecture(s) : ({index} of {total})\n".format(index=inner_index, total=lectures_count))
			sys.stdout.write(fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Downloading (%s)\n" % (lecture_title))
			try:
				retval = lecture_best.download(filepath=filepath, quiet=True, callback=self.show_progress)
			except KeyboardInterrupt:
				sys.stdout.write (fc + sd + "\n[" + fr + sb + "-" + fc + sd + "] : " + fr + sd + "User Interrupted..\n")
				sys.exit(0)
			msg = retval.get('msg')
			if msg == 'already downloaded':
				sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Lecture : '%s' " % (lecture_title) + fy + sb + "(already downloaded).\n")
			elif msg == 'download':
				sys.stdout.write (fc + sd + "[" + fm + sb + "+" + fc + sd + "] : " + fg + sd + "Downloaded  (%s)\n" % (lecture_title))
			else:
				sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Lecture : '%s' " % (lecture_title) + fc + sb + "(download skipped).\n")
				sys.stdout.write (fc + sd + "[" + fr + sb + "-" + fc + sd + "] : " + fr + sd + "{}\n".format(msg))

	def download_captions_only(self, subtitle='', filepath=''):
		if subtitle:
			self.download_subtitles(subtitle=subtitle, filepath=filepath)

	def download_lectures_only(self, lecture_best='', lecture_title='', inner_index='', lectures_count='', filepath=''):
		if lecture_best:
			self.download_lectures(lecture_best=lecture_best, lecture_title=lecture_title, inner_index=inner_index, lectures_count=lectures_count, filepath=filepath)

	def download_lectures_and_captions(self, lecture_best='', lecture_title='', inner_index='', lectures_count='', subtitle='', filepath=''):
		if lecture_best:
			self.download_lectures(lecture_best=lecture_best, lecture_title=lecture_title, inner_index=inner_index, lectures_count=lectures_count, filepath=filepath)
		if subtitle:
			self.download_subtitles(subtitle=subtitle, filepath=filepath)

	def course_download(self, path='', quality='', caption_only=False, skip_captions=False, folder_prefix='', add_date_prefix=False, add_author_suffix=False, add_course_length_suffix=False):
		if self.cookies:
			sys.stdout.write(fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Trying to login using cookies ...\n")
		if not self.cookies:
			if not self.organization:
				sys.stdout.write(fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Trying to login as " + fm + sb +"(%s)" % (self.username) +  fg + sb +"...\n")
			if self.organization:
				sys.stdout.write(fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Trying to login as organization " + fm + sb +"(%s)" % (self.organization) +  fg + sb +"...\n")
		course = lynda.course(url=self.url, username=self.username, password=self.password, organization=self.organization, cookies=self.cookies)
		course_id = course.id
		course_name = course.title
		course_thumbnail = course.thumbnail
		folder_prefix_str = ''
		chapters = course.get_chapters()
		total_lectures = course.lectures
		total_chapters = course.chapters
		assets = course.assets
		author = course.author
		author_thumbnail = course.author_thumbnail
		author_suffix_str = ''
		date_original = course.date_original
		date_updated = course.date_updated
		date_prefix_str = ''
		course_duration = course.duration_secs
		course_length_suffix_str = ''

		if folder_prefix:
			folder_prefix_str = folder_prefix

		if course.duration_secs:
			course_duration = int(course.duration_secs)
			course_duration_file = course_duration
			(mins, secs) = divmod(course_duration, 60)
			(hours, mins) = divmod(mins, 60)
			if hours == 0:
				course_duration = "%02d:%02d" % (mins, secs)
				course_duration_file = "%02dm%02ds" % (mins, secs)
			else :
				course_duration = "%02d:%02d:%02d" % (hours, mins, secs)
				course_duration_file = "%02dh%02dm%02ds" % (hours, mins, secs)

		if add_course_length_suffix:
			course_length_suffix_str = "-[" + course_duration_file + "]"
		
		if add_author_suffix:
			author_suffix_str = "-[" + "".join(x if re.compile(r'[^\\/:*?"<>|\s]').match(x) else "." for x in author) +"]"

		course_dates_path = datetime.datetime.strptime(date_updated, '%d/%m/%Y').date().strftime(folder_prefix_str + '%Y%m%d') + "-"	

		if date_updated != date_original:
			course_dates_path += "["+datetime.datetime.strptime(date_original, '%d/%m/%Y').date().strftime('%Y%m%d') +"]-"

		if add_date_prefix:
			date_prefix_str = course_dates_path #date and prefix
		else :
			date_prefix_str = folder_prefix_str #just prefix
		
		course_name_path = date_prefix_str + "".join(x if re.compile(r'[^\\/:*?"<>|_\s]').match(x) else "." for x in course_name) + author_suffix_str + course_length_suffix_str
		course_name_path = course_name_path.replace('..','.')
		
		sys.stdout.write ("\n" + fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Course ID: " + fw + sb + "%s\n" % (course_id))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Course URL: " + fw + sb + "%s\n" % (self.url))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Course Folder: " + fw + sb + "%s\n" % (course_name_path))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Course " + fw + sb + "%s\n" % (course_name.replace('_','')))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Course Thumbnail: " + fw + sb + "%s\n" % (course_thumbnail))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Author: " + fw + sb + "%s\n" % (author))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Author Thumbnail: " + fw + sb + "%s\n" % (author_thumbnail))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Originally Released: " + fw + sb + "%s\n" % (date_original))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sb + "Updated: " + fw + sb + "%s\n" % (date_updated))
		sys.stdout.write (fc + sd + "[" + fm + sb + "+" + fc + sd + "] : " + fg + sd + "Duration: " + fw + sb + "%s\n" % (course_duration))
		sys.stdout.write (fc + sd + "[" + fm + sb + "+" + fc + sd + "] : " + fg + sd + "Chapter(s): " + fw + sb + "%s\n" % (total_chapters))
		sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Lecture(s): " + fw + sb + "%s\n" % (total_lectures))
		
		if path:
			if '~' in path:
				path = os.path.expanduser(path)
			course_path = "%s\\%s" % (path, course_name_path) if os.name == 'nt' else "%s/%s" % (path, course_name_path)
		else:
			path = os.getcwd()
			course_path = "%s\\%s" % (path, course_name_path) if os.name == 'nt' else "%s/%s" % (path, course_name_path)

		course.course_description(filepath=course_path)

		#download Course and Author thumbnails
		self.download_image(course_thumbnail, os.path.join(course_path, 'course_thumbnail.jpg'))
		self.download_image(author_thumbnail, os.path.join(course_path, 'author_thumbnail.jpg'))
		
		for chapter in chapters:
			chapter_id = chapter.id
			chapter_index = chapter.index
			chapter_title = chapter.title
			chapter_title_path = "".join(x if re.compile(r'[^\\/:*?"<>|\s]').match(x) else "." for x in chapter.title)
			lectures = chapter.get_lectures()
			lectures_count = chapter.lectures
			filepath = "%s\\%s" % (course_path, chapter_title_path) if os.name == 'nt' else "%s/%s" % (course_path, chapter_title_path)
			status = course.create_chapter(filepath=filepath)
			sys.stdout.write (fc + sd + "\n[" + fm + sb + "*" + fc + sd + "] : " + fm + sb + "Downloading chapter : ({index} of {total})\n".format(index=chapter_index, total=total_chapters))
			sys.stdout.write (fc + sd + "[" + fw + sb + "+" + fc + sd + "] : " + fw + sd + "Chapter (%s)\n" % (chapter_title))
			sys.stdout.write (fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Found (%s) lectures ...\n" % (lectures_count))
			for lecture in lectures:
				lecture_id = lecture.id
				lecture_index = lecture.index
				lecture_title = lecture.title
				lecture_subtitles = lecture.subtitles
				lecture_best = lecture.getbest()
				lecture_streams = lecture.streams
				if caption_only and not skip_captions:
					self.download_captions_only(subtitle=lecture_subtitles, filepath=filepath)
				elif skip_captions and not caption_only:
					lecture_best = lecture.get_quality(best_quality=lecture_best, streams=lecture_streams, requested=quality)
					self.download_lectures_only(lecture_best=lecture_best, lecture_title=lecture_title, inner_index=lecture_index, lectures_count=lectures_count, filepath=filepath)
				else:
					lecture_best = lecture.get_quality(best_quality=lecture_best, streams=lecture_streams, requested=quality)
					self.download_lectures_and_captions(lecture_best=lecture_best, lecture_title=lecture_title, inner_index=lecture_index, lectures_count=lectures_count, subtitle=lecture_subtitles, filepath=filepath)
		if assets and len(assets) > 0:
			for asset in assets:
				self.download_assets(assets=asset, filepath=course_path)

def main():
	sys.stdout.write(banner())
	version     = "%(prog)s {version}".format(version=__version__)
	description = 'A cross-platform python based utility to download courses from lynda for personal offline use.'
	parser = argparse.ArgumentParser(description=description, conflict_handler="resolve")
	parser.add_argument('course', help="Lynda course or file containing list of courses.", type=str)
	general = parser.add_argument_group("General")
	general.add_argument(
		'-h', '--help',\
		action='help',\
		help="Shows the help.")
	general.add_argument(
		'-v', '--version',\
		action='version',\
		version=version,\
		help="Shows the version.")

	authentication = parser.add_argument_group("Authentication")
	authentication.add_argument(
        '-k', '--cookies',\
        dest='cookies',\
        type=str,\
        help="Cookies to authenticate with.",metavar='')
	authentication.add_argument(
        '-cf', '--credentials-file',\
        dest='credentials',\
        type=str,\
        help="Credentials file to authenticate with. DISCLAIMER: this is not as safe option as using cookies, but more convenient in some cases. Do not commit or share this file. Inside the file put the username on first line and password on the second.",metavar='')
	authentication.add_argument(
		'-u', '--username',\
		dest='username',\
		type=str,\
		help="Username or Library Card Number.",metavar='')
	authentication.add_argument(
		'-p', '--password',\
		dest='password',\
		type=str,\
		help="Password or Library Card Pin.",metavar='')
	authentication.add_argument(
		'-o', '--organization',\
		dest='org',\
		type=str,\
		help="Organization, registered at Lynda.",metavar='')

	advanced = parser.add_argument_group("Advanced")
	advanced.add_argument(
		'-d', '--directory',\
		dest='output',\
		type=str,\
		help="Download to specific directory.",metavar='')
	advanced.add_argument(
		'-q', '--quality',\
		dest='quality',\
		type=int,\
		help="Download specific video quality.",metavar='')

	advanced.add_argument(
		'-fp', '--folder-prefix',\
		dest='folderprefix',\
		type=str,\
		help="Prefix each course folder with custom string.",metavar='')
	
	advanced.add_argument(
		'--date-prefix',\
		dest='dateprefix',\
		action='store_true',\
		help="Prefix each course folder name with course update date and course original date, when available. Comes after the -fp prefix.")

	advanced.add_argument(
		'--author-suffix',\
		dest='authorsuffix',\
		action='store_true',\
		help="Add a suffix of the Author name in the [FirstName.LastName] format, to each course folder name with. Comes before the --course-length-suffix triggered suffix.")

	advanced.add_argument(
		'--course-length-suffix',\
		dest='courselengthsuffix',\
		action='store_true',\
		help="Add a suffix of the Length of the course in the [XXhYYmZZs] format, to each course folder name with. Comes after the --author-suffix triggered suffix.")

	other = parser.add_argument_group("Others")
	other.add_argument(
		'--info',\
		dest='info',\
		action='store_true',\
		help="List all lectures with available resolution.")
	other.add_argument(
		'--sub-only',\
		dest='caption_only',\
		action='store_true',\
		help="Download captions/subtitle only.")
	other.add_argument(
		'--skip-sub',\
		dest='skip_captions',\
		action='store_true',\
		help="Download course but skip captions/subtitle.")

	options = parser.parse_args()

	if os.path.isfile(options.course):
		f_in = open(options.course)
		courses = [line for line in (l.strip() for l in f_in) if line]
		f_in.close()
		sys.stdout.write (fc + sd + "[" + fw + sb + "+" + fc + sd + "] : " + fw + sd + "Found (%s) courses ..\n" % (len(courses)))
		for course in courses:

			if options.cookies:
				f_in = open(options.cookies)
				cookies = '\n'.join([line for line in (l.strip() for l in f_in) if line])
				f_in.close()
				lynda = Lynda(url=course, cookies=cookies)
				if options.info:
					lynda.course_list_down()

				if not options.info:
					if options.caption_only and not options.skip_captions:
						lynda.course_download(caption_only=options.caption_only, path=options.output, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
					elif not options.caption_only and options.skip_captions:
						lynda.course_download(skip_captions=options.skip_captions, path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
					else:
						lynda.course_download(path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)

			if not options.cookies:
				if options.credentials:
					f_in = open(options.credentials,'r')
					cr_lines = f_in.readlines()
					cr_user = cr_lines[0].strip()
					cr_pass = cr_lines[1].strip()
					f_in.close()					
					lynda = Lynda(url=course, username=cr_user, password=cr_pass, organization=options.org)
					if options.info:
						lynda.course_list_down()

					if not options.info:
						if options.caption_only and not options.skip_captions:
							lynda.course_download(caption_only=options.caption_only, path=options.output, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
						elif not options.caption_only and options.skip_captions:
							lynda.course_download(skip_captions=options.skip_captions, path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
						else:
							lynda.course_download(path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)

				if not options.username and not options.password and not options.credentials:
					username = fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Username : " + fg + sb
					password = fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Password : " + fc + sb
					email = getpass.getuser(prompt=username)
					passwd = getpass.getpass(prompt=password)
					if email and passwd:
						lynda = Lynda(url=course, username=email, password=passwd, organization=options.org)
					else:
						sys.stdout.write('\n' + fc + sd + "[" + fr + sb + "-" + fc + sd + "] : " + fr + sb + "Username and password is required.\n")
						sys.exit(0)

					if options.info:
						lynda.course_list_down()

					if not options.info:
						if options.caption_only and not options.skip_captions:
							lynda.course_download(caption_only=options.caption_only, path=options.output, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
						elif not options.caption_only and options.skip_captions:
							lynda.course_download(skip_captions=options.skip_captions, path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
						else:
							lynda.course_download(path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)

				elif options.username and options.password:
					lynda = Lynda(url=course, username=options.username, password=options.password, organization=options.org)
					if options.info:
						lynda.course_list_down()

					if not options.info:
						if options.caption_only and not options.skip_captions:
							lynda.course_download(caption_only=options.caption_only, path=options.output, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
						elif not options.caption_only and options.skip_captions:
							lynda.course_download(skip_captions=options.skip_captions, path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
						else:
							lynda.course_download(path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)

	if not os.path.isfile(options.course):

		if options.cookies:
			f_in = open(options.cookies)
			cookies = '\n'.join([line for line in (l.strip() for l in f_in) if line])
			f_in.close()
			lynda = Lynda(url=options.course, cookies=cookies)
			if options.info:
				lynda.course_list_down()

			if not options.info:
				if options.caption_only and not options.skip_captions:
					lynda.course_download(caption_only=options.caption_only, path=options.output, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
				elif not options.caption_only and options.skip_captions:
					lynda.course_download(skip_captions=options.skip_captions, path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
				else:
					lynda.course_download(path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)

		if not options.cookies:
			if options.credentials:
				f_in = open(options.credentials,'r')
				cr_lines = f_in.readlines()
				cr_user = cr_lines[0].strip()
				cr_pass = cr_lines[1].strip()
				f_in.close()
				lynda = Lynda(url=options.course, username=cr_user, password=cr_pass, organization=options.org)
				if options.info:
					lynda.course_list_down()

				if not options.info:
					if options.caption_only and not options.skip_captions:
						lynda.course_download(caption_only=options.caption_only, path=options.output, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
					elif not options.caption_only and options.skip_captions:
						lynda.course_download(skip_captions=options.skip_captions, path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
					else:
						lynda.course_download(path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
			if not options.username and not options.password and not options.credentials:
				username = fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Username : " + fg + sb
				password = fc + sd + "[" + fm + sb + "*" + fc + sd + "] : " + fg + sd + "Password : " + fc + sb
				email = getpass.getuser(prompt=username)
				passwd = getpass.getpass(prompt=password)
				if email and passwd:
					lynda = Lynda(url=options.course, username=email, password=passwd, organization=options.org)
				else:
					sys.stdout.write('\n' + fc + sd + "[" + fr + sb + "-" + fc + sd + "] : " + fr + sb + "Username and password is required.\n")
					sys.exit(0)

				if options.info:
					lynda.course_list_down()

				if not options.info:
					if options.caption_only and not options.skip_captions:
						lynda.course_download(caption_only=options.caption_only, path=options.output, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
					elif not options.caption_only and options.skip_captions:
						lynda.course_download(skip_captions=options.skip_captions, path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
					else:
						lynda.course_download(path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)

			elif options.username and options.password:
				lynda = Lynda(url=options.course, username=options.username, password=options.password, organization=options.org)
				if options.info:
					lynda.course_list_down()

				if not options.info:
					if options.caption_only and not options.skip_captions:
						lynda.course_download(caption_only=options.caption_only, path=options.output, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
					elif not options.caption_only and options.skip_captions:
						lynda.course_download(skip_captions=options.skip_captions, path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)
					else:
						lynda.course_download(path=options.output, quality=options.quality, folder_prefix=options.folderprefix, add_date_prefix = options.dateprefix, add_author_suffix = options.authorsuffix, add_course_length_suffix = options.courselengthsuffix)

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		sys.stdout.write ('\n' + fc + sd + "[" + fr + sb + "-" + fc + sd + "] : " + fr + sd + "User Interrupted..\n")
		sys.exit(0)